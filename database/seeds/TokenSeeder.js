'use strict'

/*
|--------------------------------------------------------------------------
| TokenSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class TokenSeeder {
  async run () {
    const token = await Factory
      .model('App/Models/Token')
      .createMany(1)
  }
}

module.exports = TokenSeeder
