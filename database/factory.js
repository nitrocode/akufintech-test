'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

const Factory = use('Factory')

Factory.blueprint('App/Models/User', async (faker, i, data) => {
  return {
    email: ['professor@localhost', 'student@localhost'][i],
    nickname: faker.username(),
    role: ['professor', 'professorAssistant'][i],
    password: 'password'
  }
})
Factory.blueprint('App/Models/Token', async (faker, i, data) => {
  return {
    token: faker.token,
    type: 'jwt_refresh_token',
    role: ['professor', 'professorAssistant'][i],
    password: 'password'
  }
})
