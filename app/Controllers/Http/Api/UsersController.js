'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/auth/src/Schemes/Session')} AuthSession */

const BaseController = require('./BaseController')
/** @type {typeof import('../../../Models/User')} */
/** @type {typeof import('../../../Models/Assignment')} */
const User = use('App/Models/User')
const UnAuthorizeException = use('App/Exceptions/UnAuthorizeException')
const Helpers = use('Helpers')
var readTextFile = require('read-text-file')
const Compare = use('Adonis/Services/Compare')

/**
 *
 * @class UsersController
 */
class UsersController extends BaseController {
  /**
   * Index
   *
   * @param {object} ctx
   * @param {AuthSession} ctx.auth
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index ({ request, response, decodeQuery }) {
    const users = await User.query(decodeQuery()).fetch()
    return response.apiCollection(users)
  }

  /**
   * Show
   *
   * @param {object} ctx
   * @param {AuthSession} ctx.auth
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show ({ request, response, instance, decodeQuery }) {
    const user = instance
    // await user.related(decodeQuery().with).load()
    return response.apiItem(user)
  }

  /**
   * Update
   *
   * @param {object} ctx
   * @param {AuthSession} ctx.auth
   * @param {Request} ctx.request
   */
  async update ({ request, response, params, instance, auth }) {
    const user = instance
    if (String(auth.user._id) !== String(user._id)) {
      throw UnAuthorizeException.invoke()
    }
    user.merge(request.only(['name', 'locale']))
    await user.save()
    return response.apiUpdated(user)
  }

  /**
   * Destroy
   *
   * @param {object} ctx
   * @param {AuthSession} ctx.auth
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ request, response, instance, auth }) {
    const user = instance
    if (String(auth.user._id) !== String(user._id)) {
      throw UnAuthorizeException.invoke()
    }
    await user.delete()
    return response.apiDeleted()
  }

  /**
   * Upload
   *
   * @param {object} ctx
   * @param {AuthSession} ctx.auth
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async upload ({ request, response, instance, auth }) {
    const user = instance
    if (String(auth.user._id) !== String(user._id)) {
      throw UnAuthorizeException.invoke()
    }
    const assignment = request.file('assignment', {
      maxSize: '2mb',
      allowedExtensions: ['txt'],
      overwrite: true
    })
    const studentName = request.input('studentName')
    const fileName = `${studentName}_${assignment.clientName}`
    await assignment.move(use('Helpers').publicPath('uploads'), { name: fileName })
    const filePath = `uploads/${fileName}`

    const contents = readTextFile.readSync(`./public/${filePath}`)
    console.log(contents.toString())
    const assignmentContent = contents.toString()
    await user.assignment().create({ fileName, filePath, studentName, assignmentContent, status: false, results: '' })
    // await user.related('assignment').load()
    return response.apiUpdated(user)
  }

  /**
   * Get assignment of user
   *
   * @param {object} ctx
   * @param {AuthSession} ctx.auth
   * @param {Request} ctx.request
   */
  async assignments ({ request, response, instance, decodeQuery }) {
    const user = instance
    const assignment = await user.assignment().query(decodeQuery()).fetch()
    return response.apiCollection(assignment)
  }

  async compare ({ request, response, instance, decodeQuery }) {
    const user = instance
    const assignmentId = request.only(['firstAssignment_id', 'secondAssignment_id'])
    const reConstruct = []
    // console.log(assignmentId.firstAssignment)
    // eslint-disable-next-line no-mixed-operators
    const assignment = await user.assignment().whereIn('_id', [assignmentId.firstAssignment_id, assignmentId.secondAssignment_id]).fetch()
    var resultArray = Object.values(JSON.parse(JSON.stringify(assignment)))
    resultArray.forEach(element => reConstruct.push(element))
    const algo1 = await Compare.similar(reConstruct[0].assignmentContent, reConstruct[1].assignmentContent)
    const algo2 = await Compare.similarity(reConstruct[0].assignmentContent, reConstruct[1].assignmentContent)
    const roundUpalgo2 = Math.round(algo2 * 10000) / 100
    const compareResult = {
      result_algo1: [
        {
          data: `I am ${algo1} That  ${reConstruct[0].studentName} and ${reConstruct[1].studentName} Copied them self's kindly investigate`,
          description: 'This Algorithm named similar uses lenght of both string to check for similarities'
        }
      ],
      result_algo2: [
        {
          data: `I am ${roundUpalgo2}% That  ${reConstruct[0].studentName} and ${reConstruct[1].studentName} Copied them self's kindly investigate`,
          description: 'This Algorithm has 2 stages first is similarity second stage is editDistance this uses both lenght charAt to compare similarities'
        }
      ]
    }
    await user.assignment().whereIn('_id', [reConstruct[0]._id]).update({
      results: compareResult.result_algo1[0],
      status: true
    })
    await user.assignment().whereIn('_id', [reConstruct[1]._id]).update({
      results: compareResult.result_algo2[0],
      status: true
    })
    return response.apiCollection(compareResult)
  }
}

module.exports = UsersController
