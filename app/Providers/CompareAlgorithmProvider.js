'use strict'

const { ServiceProvider } = require('@adonisjs/fold')
const CompareService = require('./CompareService')
class CompareAlgorithmProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register () {
    // Register our ServiceProvider on the namespace: Adonis/Services/Firebase
    // Obtain application reference: app
    this.app.bind('Adonis/Services/Compare', (app) => {
      // Obtain application configuration in config/
      // Export our service
      return new CompareService()
    })
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot () {
    //
  }
}

module.exports = CompareAlgorithmProvider
