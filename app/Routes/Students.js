'use strict'

/*
|--------------------------------------------------------------------------
| Students Routers
|--------------------------------------------------------------------------
|
*/

const Route = use('Route')

Route.group('compare', () => {
  /**
   * @swagger
   * /compare/students/{id}/upload:
   *   post:
   *     tags:
   *       - Compare
   *     summary: Upload assignment with Student Name
   *     parameters:
   *       - $ref: '#/components/parameters/Id'
   *     requestBody:
   *       required: true
   *       content:
   *         multipart/form-data:
   *           schema:
   *             type: object
   *             properties:
   *               studentName:
   *                 required: false
   *                 type: string
   *               assignment:
   *                 required: true
   *                 type: string
   *                 format: binary
   *     responses:
   *       200:
   *         description: upload success
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       403:
   *         $ref: '#/components/responses/Forbidden'
   *       422:
   *         $ref: '#/components/responses/ValidateFailed'
   */
  Route.post('/students/:id/upload', 'Api/UsersController.upload')
    .middleware(['auth:jwt'])
    .instance('App/Models/User')

  /**
   * @swagger
   * /compare/students/{id}/assignment/{assignmentId}:
   *   delete:
   *     tags:
   *       - Compare
   *     summary: Delete assignment
   *     parameters:
   *       - $ref: '#/components/parameters/Id'
   *       - name: assignmentId
   *         description: Id of assignment object
   *         in:  path
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: delete success
   *       404:
   *         $ref: '#/components/responses/NotFound'
   *       401:
   *         $ref: '#/components/responses/Unauthorized'
   *       403:
   *         $ref: '#/components/responses/Forbidden'
   */
  Route.delete('/students/:id/assignment/:assignmentId', 'Api/UsersController.deleteassignment')
    .middleware(['auth:jwt'])
    .instance('App/Models/User')

  /**
   * @swagger
   * /compare/students/{id}/assignment:
   *   get:
   *     tags:
   *       - Compare
   *     summary: Get assignments of user
   *     parameters:
   *       - $ref: '#/components/parameters/Id'
   *     responses:
   *       200:
   *         description: success
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/Newassignment'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   */
  Route.get('/students/:id/assignment', 'Api/UsersController.assignments')
    .instance('App/Models/User').middleware(['auth:jwt'])

  /**
   * @swagger
   * /compare/students/{id}/compare_assignments:
   *   post:
   *     tags:
   *       - Compare
   *     summary: Compare Assignments using mini Algorithm
   *     parameters:
   *       - $ref: '#/components/parameters/Id'
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               firstAssignment_id:
   *                 type: string
   *                 required: true
   *               secondAssignment_id:
   *                 type: string
   *                 required: true
   *     responses:
   *       200:
   *         description: success
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/Newassignment'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   */
  Route.post('/students/:id/compare_assignments', 'Api/UsersController.compare').instance('App/Models/User').validator('Compare').middleware(['auth:jwt'])
}).prefix('/api/compare')

Route.group('history', () => {
  /**
   * @swagger
   * /history/students/{id}:
   *   get:
   *     tags:
   *       - History
   *     summary: Get table/list of all previous comparison sessions
   *     parameters:
   *       - $ref: '#/components/parameters/Id'
   *     responses:
   *       200:
   *         description: success
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/Newassignment'
   *       404:
   *         $ref: '#/components/responses/NotFound'
   */
  Route.get('/students/:id', 'Api/UsersController.assignments')
    .instance('App/Models/User').middleware(['auth:jwt'])
}).prefix('/api/history')
