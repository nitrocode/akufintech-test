'use strict'
/** @type {typeof import('lucid-mongo/src/LucidMongo/Model')} */
const Model = use('Model')

/**
 * @swagger
 * components:
 *   schemas:
 *     Newassignment:
 *       type: object
 *       properties:
 *         filename:
 *           type: string
 *     assignment:
 *       allOf:
 *         - $ref: '#/components/Newassignment'
 *         - type: object
 *           properties:
 *             _id:
 *               type: string
 */
class assignment extends Model {
  static get rules () {
    return {
      filename: 'required'
    }
  }

  static boot () {
    super.boot()
    this.addHook('afterDelete', 'App/Models/Hooks/assignment.removeFile')
  }
}

module.exports = assignment
