const fs = use('fs')
const Helpers = use('Helpers')
const assignmentHooks = exports = module.exports = {}

assignmentHooks.removeFile = async (instance) => {
  const filePath = Helpers.publicPath(instance.filePath)
  fs.unlinkSync(filePath)
}
